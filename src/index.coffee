request = require 'superagent'
{omit} = require 'underscore'
basic_auth = require('express').basicAuth
{server, client, resource} = require('config').authmode


fetch_tokens = (res, next, code, callback) ->
  request.post("#{server}/access/tokens")
    .send
      grant_type: 'authorization_code'
      code: code
      redirect_uri: res.uri_for('auth-callback')
    .auth(client.identifier, client.secret)
    .on 'error', (error) ->
      return next error if error?
    .end (response) ->
      callback response.body.access_token, response.body.refresh_token 

with_identity = (callback) ->
  (req, res, next) ->
    console.log req.query
    if req.query.error?
      console.log 'Error authenticating %j', req.query.error
      return next status: 403
    fetch_tokens res, next, req.query.code, (access_token, refresh_token) ->
      request.get("#{server}/identity")
        .set('Authorization', "Bearer #{access_token}")
        .on 'error', (error) ->
          return next error if error?
        .end (response) ->
          callback response.body, access_token, req, res, next

module.exports =

  authenticate: (res, redirect_uri) ->
    redirect_uri ?= res.uri_for('auth-callback')
    res.redirect "#{server}/authz/requests/core?client_id=#{client.identifier}&redirect_uri=#{redirect_uri}&response_type=code"

  with_identity: with_identity

  handlers: (default_route, init_callback) ->
    
    callback:       

      get: with_identity (identity, access_token, req, res, next) ->
        init_callback identity, req, res, (user) ->
          req.session.user = user
          req.session.access_token = access_token
          continuation = req.flash('continue').pop()
          if continuation?
            res.redirect continuation
          else 
            res.redirect_to default_route

    logout:

      get: (req, res) ->
        req.session.user = null
        if req.session.access_token?
          request = require 'superagent'
          {server, client} = require('config').authmode
          request.del("#{server}/access/tokens")
            .set('Authorization', "Bearer #{req.session.access_token}")
            .end()
        req.session.access_token = null
        res.redirect_to default_route

  stubs:

    access:

      token:

        middleware: basic_auth (identifier, secret, callback) ->
          callback null, (identifier == resource.identifier and secret == resource.secret)

        put: (req, res, next) ->
          res.type 'text/plain'
          res.send 'Token Accepted'  

        delete: (req, res, next) ->
          res.type 'text/plain'
          res.send 'Token Deleted'
