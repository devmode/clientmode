(function() {
  var basic_auth, client, fetch_tokens, omit, request, resource, server, with_identity, _ref;

  request = require('superagent');

  omit = require('underscore').omit;

  basic_auth = require('express').basicAuth;

  _ref = require('config').authmode, server = _ref.server, client = _ref.client, resource = _ref.resource;

  fetch_tokens = function(res, next, code, callback) {
    return request.post("" + server + "/access/tokens").send({
      grant_type: 'authorization_code',
      code: code,
      redirect_uri: res.uri_for('auth-callback')
    }).auth(client.identifier, client.secret).on('error', function(error) {
      if (error != null) {
        return next(error);
      }
    }).end(function(response) {
      return callback(response.body.access_token, response.body.refresh_token);
    });
  };

  with_identity = function(callback) {
    return function(req, res, next) {
      console.log(req.query);
      if (req.query.error != null) {
        console.log('Error authenticating %j', req.query.error);
        return next({
          status: 403
        });
      }
      return fetch_tokens(res, next, req.query.code, function(access_token, refresh_token) {
        return request.get("" + server + "/identity").set('Authorization', "Bearer " + access_token).on('error', function(error) {
          if (error != null) {
            return next(error);
          }
        }).end(function(response) {
          return callback(response.body, access_token, req, res, next);
        });
      });
    };
  };

  module.exports = {
    authenticate: function(res, redirect_uri) {
      if (redirect_uri == null) {
        redirect_uri = res.uri_for('auth-callback');
      }
      return res.redirect("" + server + "/authz/requests/core?client_id=" + client.identifier + "&redirect_uri=" + redirect_uri + "&response_type=code");
    },
    with_identity: with_identity,
    handlers: function(default_route, init_callback) {
      return {
        callback: {
          get: with_identity(function(identity, access_token, req, res, next) {
            return init_callback(identity, req, res, function(user) {
              var continuation;
              req.session.user = user;
              req.session.access_token = access_token;
              continuation = req.flash('continue').pop();
              if (continuation != null) {
                return res.redirect(continuation);
              } else {
                return res.redirect_to(default_route);
              }
            });
          })
        },
        logout: {
          get: function(req, res) {
            var _ref1;
            req.session.user = null;
            if (req.session.access_token != null) {
              request = require('superagent');
              _ref1 = require('config').authmode, server = _ref1.server, client = _ref1.client;
              request.del("" + server + "/access/tokens").set('Authorization', "Bearer " + req.session.access_token).end();
            }
            req.session.access_token = null;
            return res.redirect_to(default_route);
          }
        }
      };
    },
    stubs: {
      access: {
        token: {
          middleware: basic_auth(function(identifier, secret, callback) {
            return callback(null, identifier === resource.identifier && secret === resource.secret);
          }),
          put: function(req, res, next) {
            res.type('text/plain');
            return res.send('Token Accepted');
          },
          "delete": function(req, res, next) {
            res.type('text/plain');
            return res.send('Token Deleted');
          }
        }
      }
    }
  };

}).call(this);
